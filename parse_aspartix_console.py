from parse_aspartix import *

########### Example Queries
# python parse_aspartix_console.py ground.dl metalevel.dl DEMO/firewall-paf.dl
# python parse_aspartix_console.py ground.dl metalevel.dl DEMO/eric-eaf.dl
# python parse_aspartix_console.py ground.dl metalevel.dl DEMO/gp-eaf.dl
# python parse_aspartix_console.py ground.dl metalevel.dl DEMO/bittorrent-vaf.dl
# python parse_aspartix_console.py ground.dl metalevel.dl DEMO/metalevel-paf.dl
# python parse_aspartix_console.py ground.dl metalevel.dl DEMO/metalevel-weather-eaf.dl

# multiple extensions
# python parse_aspartix_console.py prefex.dl metalevel.dl DEMO/metalevel-avaf.dl

###########
	    
# main starts here
run_aspartix_console()
