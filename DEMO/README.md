We include the following abstract examples to show the usability of the extension.
Our demo paper discusses some details about these examples [1].

*  The firewall scenario [2]: firewall-paf
*  The bittorrent scenario [2]: bittorrent-vaf
*  The hypertension scenario [3]: eric-eaf, gp-eaf
*  Other abstract examples [4]: metalevel-paf, metalevel-weather-eaf, metalevel-avaf

## Examples using grounded semantics
*  To run a particular example on the root folder of this repository, open a terminal:

`python parse_aspartix_console.py ground.dl metalevel.dl DEMO/example-name.dl`
*  You should replace example-name with one the following example names: firewall-paf, 
eric-eaf, gp-eaf, bittorrent-vaf, metalevel-paf, metalevel-weather-eaf

## Examples using preferred semantics -- multiple extensions
*  There is one such example provided: metalevel-avaf.dl

`python parse_aspartix_console.py prefex.dl metalevel.dl DEMO/metalevel-avaf.dl`

## Outputs
The outputs are displayed on Terminal as a list of justified arguments, and a MAF 
is generated as an image file to display the computed MAF as a directed graph.

*  After running each command for an example, an image file will be generated
in the DEMO folder. The outputs that you get should be the same as provided
in the 'outputs' folder.

# REFERENCES
[1] Kokciyan, N., Sassoon, I., Young, A. P., Modgil, S., & Parsons, S. (2018). 
Reasoning with metalevel argumentation frameworks in aspartix. Frontiers in 
Artificial Intelligence and Applications, 305, 463-464.

[2] A. Applebaum, Z. Li, K. Levitt, S. Parsons, J. Rowe, and E. I. Sklar. 
Firewall configuration: An application of multiagent metalevel argumentation. 
Argument & Computation, 7(2-3):201–221, 2016.

[3] N. Kokciyan, I. Sassoon, A. Young, M. Chapman, T. Porat, M. Ashworth, V. Curcin, 
S. Modgil, S. Parsons, and E. Sklar. Towards an Argumentation System for Supporting 
Patients in Self-Managing their Chronic Conditions. 2018.

[4] S. Modgil and T. J. Bench-Capon. Metalevel argumentation. Journal of Logic and Computation,
21(6):959–1003, 2011.