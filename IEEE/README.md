We introduce three examples to demonstrate how to use our extension
to generate **structured** MAFs. 

These three examples are as described in our paper:
"Applying Metalevel Argumentation Frameworks to Support Medical Decision Making"
by Nadin Kokciyan, Isabel Sassoon, Elizabeth Sklar, Sanjay Modgil and Simon Parsons. **[Under review.]**

* jane-kb.dl includes all information in the knowledge base as represented
in Table 1 in the paper.
* The factual information about each example is provided in jane-ex files.
* The execution of the commands will also produce a PNG file in IEEE folder
* The outputs should be same as the ones in 'outputs' folder. 

# Example 1: Use of NG136 guideline
*  In the root folder, the following command should be executed on Terminal:
 
`python parse_aspartix_console.py ground.dl metalevel.dl IEEE/jane-kb.dl IEEE/jane-ex1.dl`

# Example 2 Use of NHS Choices
*  In the root folder, the following command should be executed on Terminal:
 
`python parse_aspartix_console.py ground.dl metalevel.dl IEEE/jane-kb.dl IEEE/jane-ex2.dl`

# Example 3: Explanations
*  In the root folder, the following command should be executed on Terminal:

`python parse_aspartix_console.py ground.dl metalevel.dl IEEE/jane-kb.dl IEEE/jane-ex3.dl`