This implementation includes three examples as discussed in our paper 
"Applying Metalevel Argumentation Frameworks to Support Medical Decision Making"
by Nadin Kokciyan, Isabel Sassoon, Elizabeth Sklar, Sanjay Modgil and Simon Parsons. 
Our paper has been accepted to *IEEE Intelligent Systems*.

* IEEE folder contains three examples of Jane to show how different information
sources can be used to construct structured metalevel argumentation frameworks.  

* DEMO folder contains more examples to show the usability of our extension. 
Some details are discussed in our demo paper [1], we are now sharing our code
in this repository.

* Each folder contains a subfolder 'outputs' where the generated MAFs are included.
After running each command, a MAF image file will be generated. Please check the
requirements.txt to install the necessary packages.

# Metalevel Argumentation Framework (MAF) extension for ASPARTIX
We introduce a metalevel extension (metalevel.dl)[1] that can be plugged into the 
existing ASPARTIX framework. We developed a Python code (parse_aspartix_console.py) 
to process the DLV solver results. The outputs are displayed on Terminal, and a MAF 
is generated as an image file to display the computed MAF as a directed graph. 

We use [DLV](http://www.dlvsystem.com/dlv/) as the ASP-solver to compute 
extensions under the chosen semantics (grounded, preferred and such. 

# ASPARTIX
ASPARTIX is an ASP-based argumentation system to evaluate Dung style argumentation
frameworks. 

* ground.dl and prefex.dl are extensions developed as part of ASPARTIX.
* ground.dl will compute the grounded extension, and the prefex.dl will compute the preferred 
extensions. 
* Please refer to ASPARTIX [website](https://www.dbai.tuwien.ac.at/proj/argumentation/systempage/) to access more semantics.

# REQUIREMENTS

*  [DLV](http://www.dlvsystem.com/dlv/) reasoner should be installed. dlv script 
should be located in the code folder. The script is already included in the root
folder for your convenience. Before running the code, make sure that this file
is an executable file on your system. 
*  The code was tested with Python 2.7.16 on a Linux machine (Ubuntu 18.04.4 LTS) 
and on a Mac machine (macOS Mojave 10.14.6).
*  requirements.txt includes more information about the required packages.

# REFERENCES
[1] Kokciyan, N., Sassoon, I., Young, A. P., Modgil, S., & Parsons, S. (2018). 
Reasoning with metalevel argumentation frameworks in aspartix. Frontiers in 
Artificial Intelligence and Applications, 305, 463-464.
